import vds_ws from 'vds_ws/vds_ws';
import
{
  user_credentials_to_key,
  decrypt_private_key,
  parse_public_key,
  public_key_fingerprint,
  base64,
  hash_sha256_begin,
  hash_sha256_update,
  hash_sha256_finish  
} from 'vds_ws/vds_crypto';

const vdsApiLoginType = 'VDS_API_LOGIN';
const vdsApiLoginedType = 'VDS_API_LOGINED';

const vdsApiInProgressType = 'VDS_API_IN_PROGRESS';
const vdsApiErrorType = 'VDS_API_ERROR';
const vdsApiClearErrorType = 'VDS_API_CLEAR_ERROR';

const vdsApiPersonalMessageType = 'VDS_API_PERSONAL_MESSAGE';


const vdsApiChannelSelectType = 'VDS_API_CHANNEL_SELECT_MESSAGE';
const vdsApiChannelMessageType = 'VDS_API_CHANNEL_MESSAGE';

const vdsApiUploadFileType = 'VDS_API_UPLOAD_FILE';
const vdsApiUploadingFileType = 'VDS_API_UPLOADING_FILE';
const vdsApiUploadFileErrorType = 'VDS_API_UPLOAD_ERROR_FILE';

const initialState =
{
  vdsApiState: 'offline',
  vdsApiWebSocket: null,
  vdsApiChannels: new Map(),
  vdsApiChannelFiles: new Map(),

  vdsApiInProgress: false,
  vdsApiError: '',

  vdsApiUploading: '',
  vdsApiUploaded: 0,
  vdsApiUploadSize: 0,
  vdsApiUploadError: ''
};

async function get_ws(dispatch, getState)
{
  var ws = getState().vdsApi.vdsApiWebSocket;
  if(!ws){
    ws = new vds_ws();
    const keys = await ws.invoke('login', [ sessionStorage.getItem('email'), user_credentials_to_key(sessionStorage.getItem('password')) ]);
    const private_key = decrypt_private_key(keys.private_key, sessionStorage.getItem('password')); 
    const public_key = parse_public_key(keys.public_key);
    const public_key_id = base64(public_key_fingerprint(public_key));

    ws.add_read_key(public_key_id, { public_key, private_key });
    ws.add_write_key(public_key_id, { public_key, private_key });

    dispatch({ type: vdsApiLoginedType, ws: ws, keys: { public_key: public_key, private_key: private_key } })

    var messages = await ws.invoke('get_channel_messages', [public_key_id]);
    messages.forEach(message => {
      var msg = ws.decrypt(message);
      switch(msg.type){
        case 'channel_create': {
          dispatch({ type: vdsApiPersonalMessageType, channel: msg.channel });
          break;
        }
        default:
          break;
      }
    });
  }

  return ws;
}

async function load_channel_messages_func(dispatch, getState, channel_id) {
  try
  {
    dispatch({ type: vdsApiChannelSelectType, channel: channel_id });

    const ws = await get_ws(dispatch, getState);
    var channel = getState().vdsApi.vdsApiChannels.get(channel_id);
    var messages = await ws.invoke('get_channel_messages', [channel_id]);
    messages.forEach(item => {
      var item_msg = channel.decrypt(item);
      dispatch({ type: vdsApiChannelMessageType, message: item_msg });
    });
  }
  catch(ex){
    console.log('error', ex);
    dispatch({ type: vdsApiErrorType, error: ex });
  }
}


  export const actionCreators = {
    refreshConnection: () => async(dispatch, getState) => {
      if(sessionStorage.getItem('email') && sessionStorage.getItem('password')) {
        try
        {
          await get_ws(dispatch, getState);
        }
        catch(ex){
          console.log('error', ex);
          dispatch({ type: vdsApiErrorType, error: ex });
        }  
      }
    },

    login: (email, password) => async(dispatch, getState) => {
      try{
        dispatch({ type: vdsApiLoginType });
        sessionStorage.setItem('email', email);
        sessionStorage.setItem('password', password);

        await get_ws(dispatch, getState);
        dispatch({ type: vdsApiInProgressType, inProgress: false });
      }
      catch(ex){
        console.log('error', ex);
        dispatch({ type: vdsApiErrorType, error: ex });
      }
    },

  clearError: () => (dispatch) => {
    dispatch({ type: vdsApiClearErrorType });
  },

  register: (email, password) => async(dispatch, getState) => {
    dispatch({ type: vdsApiInProgressType, inProgress: true });
    try{
      let ws = new vds_ws();
      await ws.invoke('login', [ email, user_credentials_to_key(password) ]);

      dispatch({ type: vdsApiErrorType, error: 'Пользователь уже существует' });
      return;
    }
    catch(ex) {
      if(ex === 'Invalid password'){
        dispatch({ type: vdsApiErrorType, error: 'Пользователь уже существует' });
        return;
      }
    }

    try{
      let ws = new vds_ws();
      await ws.register_user(email, password);

      sessionStorage.setItem('email', email);
      sessionStorage.setItem('password', password);

      await get_ws(dispatch, getState);  
      dispatch({ type: vdsApiInProgressType, inProgress: false });
    }
    catch(ex){
      console.log('error', ex);
      dispatch({ type: vdsApiErrorType, error: ex });
    }
  },

  load_channel_messages: (channel_id) => async(dispatch, getState) => {
    await load_channel_messages_func(dispatch, getState, channel_id);
  },

  upload: (channel_id, files) => async(dispatch, getState) => {

    Array.from(files).forEach(async file => {

      console.log(file);

      const fileSize   = file.size;
      const chunkSize  = 67108864;
      var offset = 0;
      var chunkReaderBlock = null;
      var file_blocks = [];
      var md = hash_sha256_begin();
      const ws = await get_ws(dispatch, getState);


      var readEventHandler = async function(evt) {
          if (evt.target.error == null) {
              offset += evt.target.result.byteLength;
              try
              {
                hash_sha256_update(md, evt.target.result);
                console.log('save_block result 1');
                const result = await ws.save_block(evt.target.result);
                console.log('save_block result', result);
                file_blocks.push(result);
                dispatch({ type: vdsApiUploadingFileType, uploaded: offset });
              }
              catch(ex)
              {
                dispatch({ type: vdsApiUploadFileErrorType, error: ex });
                return;
              }
            } else {
              dispatch({ type: vdsApiUploadFileErrorType, error: evt.target.error });
              return;
          }
          if (offset >= fileSize) {
            const file_id = hash_sha256_finish(md);
            const channel = getState().vdsApi.vdsApiChannels.get(channel_id);

            await ws.invoke(
              'broadcast',
              [base64(channel.crypt(channel.serialize_message(
                '{"$type":"SimpleMessage"}',
                [{
                  file_name: file.name,
                  mime_type: file.type || 'application/octet-stream',
                  file_size: file.size,
                  file_id : file_id,
                  file_blocks: file_blocks
                }])))]);
            dispatch({ type: vdsApiUploadFileType, name: '', size: 0 });

            await load_channel_messages_func(dispatch, getState, channel_id);
            return;
          }

          // of to the next chunk
          chunkReaderBlock(offset, chunkSize, file);
      }

      chunkReaderBlock = function(_offset, length, _file) {
          var r = new FileReader();
          var blob = _file.slice(_offset, length + _offset);
          r.onload = readEventHandler;
          r.readAsArrayBuffer(blob);
      }

      dispatch({ type: vdsApiUploadFileType, name: file.name, size: file.size });
      chunkReaderBlock(offset, chunkSize, file);
    });
  },
  download: (file_block) => async(dispatch, getState) => {
    try
    {
      const ws = await get_ws(dispatch, getState);
      return await ws.download(file_block);
    }
    catch(ex){
      dispatch({ type: vdsApiErrorType, error: ex });
    }
  },
  create_channel: (channel_type, channel_name) => async(dispatch, getState) => {
    dispatch({ type: vdsApiInProgressType, inProgress: true });
    try
    {
      const ws = await get_ws(dispatch, getState);
      const channel_id = await ws.channel_create(channel_type, channel_name);

      dispatch({ type: vdsApiInProgressType, inProgress: false });

      return channel_id;
    }
    catch(ex){
      dispatch({ type: vdsApiErrorType, error: ex });
    }
  }
};

export const reducer = (state, action) => {
  state = state || initialState;

  switch (action.type) {
    case vdsApiLoginType:
        {
          console.log('vdsApiLoginType');
          return { ...state, vdsApiState: 'login', vdsApiInProgress: true, vdsApiError: '', vdsApiWebSocket: null };
        }
    case vdsApiLoginedType:
      {
        return { ...state, vdsApiState: 'logined',  vdsApiWebSocket: action.ws, vdsApiKeys: action.keys, vdsApiChannels: new Map() };
      }
    case vdsApiInProgressType:
      {
        return { ...state, vdsApiInProgress: action.inProgress,  vdsApiError: '' };
      }
    case vdsApiErrorType:
      {
        return { ...state, vdsApiInProgress: false,  vdsApiError: '' + action.error };
      }
    case vdsApiClearErrorType:
    {
      return { ...state, vdsApiError: '' };
    }

    case vdsApiPersonalMessageType:
    {
      var newChannels = new Map(state.vdsApiChannels);
      newChannels.set(action.channel.channel_id, action.channel);

      return { ...state, vdsApiChannels: newChannels };
    }

    case vdsApiChannelSelectType:
    {
      return { ...state, vdsApiChannel: action.channel_id, vdsApiChannelMessages: [], vdsApiChannelFiles: new Map() };
      
    }
    case vdsApiChannelMessageType:
    {
      var messages = new Array(state.vdsApiChannelMessages);
      messages.push(action.message);

      var files = new Map(state.vdsApiChannelFiles);

      if('user_message_transaction' === action.message.type){
        action.message.body.files.forEach(file => {
          if(!files.has(file.name)){
            files.set(file.name, [file]);
          }
          else{
            files.get(file.name).push(file);
          }
        });
      }
      return { ...state, vdsApiChannelMessages: messages, vdsApiChannelFiles: files};
    }

    case vdsApiUploadFileType:
    {
      return { ...state, vdsApiUploading: action.name, vdsApiUploaded: 0, vdsApiUploadSize: action.size, vdsApiUploadError: '' };
    }

    case vdsApiUploadingFileType:
    {
      return { ...state, vdsApiUploaded: action.uploaded };
    }

    case vdsApiUploadFileErrorType:
    {
      return { ...state, vdsApiUploading: '', vdsApiUploadError: action.error };
    }
    default:
      break;
  }

  return state;
};
