import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { actionCreators } from "../../store/vds_api";
import { TextField, Button, Grid, CircularProgress, Snackbar, SnackbarContent, Icon } from '@material-ui/core';
import WarningIcon from '@material-ui/icons/Warning';
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    iconVariant: {
        opacity: 0.9
    },
    warning: {
        backgroundColor: theme.palette.error.dark,
      },
});

class RegisterPage extends React.Component{
    state = {
        userEmail: '',
        userPassword: '',
        confirmPassword: '',
        showError: true
      };
    
    handleRegister = async () => {
        this.setState({showError:true});
        await this.props.register(this.state.userEmail, this.state.userPassword);

        if('logined' === this.props.vdsApiState){
            this.props.history.push('/app');
        }
    }

    handleClose = () => {
        this.setState({showError:false});
    }

     handleChange = (name) => event => {
        this.setState({ [name]: event.target.value });
    };

    render() {
        const { classes } = this.props;
        const { showError } = this.state;
        const loading = (this.props.vdsApiState === 'login');
        const loginError = this.props.vdsApiError;
        const showSnackbar = (showError && loginError !== '');

        return (
            <Grid container>
                <Grid item xs={12}>
                    <Grid container justify="center">
                        <Grid item>
                            <h1>Регистрация в систему</h1>
                            <p>Укажите адрес электронной почты и пароль, для регистрации в системе:</p>
                            <TextField
                                id="standard-required"
                                placeholder="адрес электронной почты"
                                value={this.state.userEmail}
                                onChange={this.handleChange('userEmail')}
                            /><br/>
                            <TextField
                                type="password"
                                placeholder="пароль"
                                value={this.state.userPassword}
                                onChange={this.handleChange('userPassword')}
                             /><br/>
                            <TextField
                                type="password"
                                placeholder="повторите пароль"
                                value={this.state.confirmPassword}
                                onChange={this.handleChange('confirmPassword')}
                             />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container justify="center">
                        <Grid item>
                            {loading && <CircularProgress  className={classes.fabProgress} />}
                            <Button onClick={this.handleRegister}>Регистрация</Button>
                            <SnackbarContent
                                className={classes.warning}
                                aria-describedby="client-snackbar"
                                message={
                                    <span id="client-snackbar" className={classes.message}>
                                    <Icon className={WarningIcon} />
                                    Внимание! Это демо сервер: все данные и пользователи удаляются периодически.
                                    </span>
                                }
                                />
                            <Snackbar
                                anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                                }}
                                open={showSnackbar}
                                autoHideDuration={6000}
                                onClose={this.handleClose}
                            >
                                <SnackbarContent
                                className={classes.error}
                                aria-describedby="client-snackbar"
                                message={
                                    <span id="client-snackbar" className={classes.message}>
                                    <Icon className={classes.iconVariant} />
                                    {this.props.vdsApiError}
                                    </span>
                                }
                                />
                            </Snackbar>

                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}
export default connect(
    state => state.vdsApi,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )(withStyles(styles, { withTheme: true })(RegisterPage));
  
