import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { actionCreators } from "../../store/vds_api";
import { Link } from 'react-router-dom';
import { TextField, Button, Grid, CircularProgress, Typography } from '@material-ui/core';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    iconVariant: {
        opacity: 0.9
    },
});

class LoginPage extends React.Component{
    state = {
        userEmail: '',
        userPassword: ''
      };
    
    handleLogin = async () => {
        await this.props.login(this.state.userEmail, this.state.userPassword);

        if('logined' === this.props.vdsApiState){
            this.props.history.push('/app');
        }
    }

    handleChange = (name) => event => {
        this.setState({ [name]: event.target.value });
    };

    render() {
        const { classes } = this.props;
        const loading = this.props.vdsApiInProgress;

        return (
            <Grid container>
                <Grid item xs={12}>
                    <Grid container justify="center">
                        <Grid item>
                            <h1>Вход в систему</h1>
                            <p>Укажите адрес электронной почты и пароль, указанные при регистрации:</p>
                            <TextField
                                id="standard-required"
                                placeholder="адрес электронной почты"
                                value={this.state.userEmail}
                                onChange={this.handleChange('userEmail')}
                            /><br/>
                            <TextField
                                type="password"
                                placeholder="пароль"
                                value={this.state.userPassword}
                                onChange={this.handleChange('userPassword')}
 />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container justify="center">
                        <Grid item>
                            {loading && <CircularProgress  className={classes.fabProgress} />}
                            <Button onClick={this.handleLogin} disabled={loading}>Войти</Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container justify="center">
                        <Grid item>
                            <Typography>Если Вы ещё не зарегистрировались, пожалуйста <Link to='/register'>зарегистрируйтесь</Link></Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}
export default connect(
    state => state.vdsApi,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )(withStyles(styles, { withTheme: true })(LoginPage));
  
