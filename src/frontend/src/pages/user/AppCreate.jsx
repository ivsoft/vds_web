import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { actionCreators } from "../../store/vds_api";
import { withStyles } from '@material-ui/core/styles';
import { FormControl, InputLabel, Select, MenuItem, Input, Button, CircularProgress } from '@material-ui/core';

const styles = theme => ({
});

class AppCreate extends React.Component {
    constructor(props) {
      super(props);
      this.body = props.children;
    }

    state = {
        type: 'core.file',
        name: 'Мои файлы'
    };

    handleChange = (name) => event => {
        this.setState({ [name]: event.target.value });
    };

    handleCreate = async () => {
        var channel_id = await this.props.create_channel(this.state.type, this.state.name);

        if('' === this.props.vdsApiError){
            this.props.history.push('/app/' + encodeURIComponent(channel_id));
        }
    }

    render() {
        const { classes } = this.props;
        const loading = this.props.vdsApiInProgress;
        console.log('loading', loading);
        return (
            <div>
                <h1>Создание приложения:</h1>
                <form className={classes.root} autoComplete="off">
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="app-type">Тип приложения</InputLabel>
                        <Select
                            value={this.state.type}
                            onChange={this.handleChange('type')}
                            inputProps={{
                                name: 'type',
                                id: 'app-type',
                            }}
                        >
                        <MenuItem value={'core.file'}>Хранилище файлов</MenuItem>
                        <MenuItem value={'core.notes'}>Мессенджер</MenuItem>
                        <MenuItem value={'core.news'}>Лента новостей</MenuItem>
                        </Select>
                    </FormControl>
                    <br/>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="app-name">Имя приложения</InputLabel>
                        <Input 
                            id="app-name"
                            placeholder="Имя приложения"
                            value={this.state.name}
                            onChange={this.handleChange('name')}
                        />
                    </FormControl>
                    <br/>
                    <Button variant="contained" onClick={this.handleCreate} disabled={loading}>Создать</Button>
                    {loading && <CircularProgress className={classes.fabProgress} />}
                </form>
            </div>
        );
    }
}

AppCreate.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
  };
  
  export default connect(
    state => state.vdsApi,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )(withStyles(styles, { withTheme: true })(AppCreate));
  