import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { actionCreators } from "../../store/vds_api";
import { withStyles } from '@material-ui/core/styles';
import AppList from '../../components/AppList';
import { Fab } from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';

const styles = theme => ({
});

class Channels extends React.Component {
    constructor(props) {
      super(props);
      this.body = props.children;
    }

    handleAdd = () => {
       this.props.history.push('/app/create');
    }


    render() {
        // const { classes, theme } = this.props;
        return (
            <div>
            <h1>Приложения</h1>
            <p>Доступны следующие приложения:</p>
            <AppList />
            <Fab color="primary" aria-label="add" onClick={this.handleAdd}>
              <AddIcon />
            </Fab>
          </div>
        
        );
    }
}

Channels.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
  };
  
  export default connect(
    state => state.vdsApi,
    dispatch => bindActionCreators(actionCreators, dispatch)
  )(withStyles(styles, { withTheme: true })(Channels));
  