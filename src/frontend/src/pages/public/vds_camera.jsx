import React from 'react';
import { connect } from 'react-redux';

const vds_camera = props => (
    <div>
    <h1>Камера безопасности</h1>
    <p>
        Описывается настройка камеры безопасности
    </p>
    <p>
        Если у вас есть IP камера и устройтво типа Rasberry PI Вы можете настроить
        систему безопасности вашего дома для хранения снятых файлов в виртуальной
        сети хранения.
    </p>
    <p>
        Для этого:
        <ul>
            <li>Установите и настройте камеру,</li>
            <li>Установите на Rasberry PI Ubuntu,</li>
            <li>Скачайте агента виртуальной сети хранения</li>
            <li>Скачайте Motion и настройте его на отслеживание вашей камеры</li>
        </ul>
    </p>
    <p>
        Выделите место на диске под хранение кэша файлов:
        <blockquote>vds_cmd storage add -l ващ_логин -p ваш_пароль --storage-path путь_до_кэша --storage-size размер_кэша</blockquote>
    </p>
    <p>
        Создайте новый канал:
        <blockquote>vds_cmd channel create -l ваш_логин -p ваш_пароль -cn Camera -ct core.files</blockquote>
        Получите список канолов и запомните ID канала:
        <blockquote>vds_cmd channel list -l ваш_логин -p ваш_пароль</blockquote>
    </p>
    <p>
        Создайте bash скрипт для отправки файлов в сеть:
        <blockquote>
        #!/bin/bash<br/>
        vds_cmd file sync -l ваш_логин -p ваш_пароль -c ID_канала -ss upload -o /var/lib/motion/<br/>
        </blockquote>
    </p>
    <p>
        Установите таймер для выполнения скрипта раз в 10 минут:
        <blockquote>*/10 * * * * /bin/send_pics.sh</blockquote>
    </p>
    <p>Запустите Motion и проверьте, что раз в 10 минут в виртуальную сеть заливаются и надёжно хранятся снимки с вашей камеры</p>

    </div>
);

export default connect()(vds_camera);
