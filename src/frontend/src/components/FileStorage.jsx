import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import UploadPanel from './UploadPanel';
import PropTypes from 'prop-types';
import FileStorageFiles from './FileStorageFiles';

const styles = theme => ({
});

class FileStorage extends React.Component {
    constructor(props) {
        super(props);
        this.body = props.children;
    }

    render() {
        // const { classes, theme } = this.props;
        
        return (
            <div>
                <FileStorageFiles {...this.props}/> 
                <UploadPanel {...this.props}/>
            </div>
        );
    }
}
  
FileStorage.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default connect(
)(withStyles(styles, { withTheme: true })(FileStorage));
