import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { actionCreators } from "../store/vds_api";
import { Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core';
import { createWriteStream } from 'streamsaver';

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    chip: {
    }
});

class FileStorageFiles extends React.Component {
    constructor(props) {
        super(props);
        this.body = props.children;
    }
    componentDidMount() {
        this.ensureDataFetched();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id === this.props.match.params.id) {
            return;
        }
        
        this.ensureDataFetched();
    }
    
    ensureDataFetched() {    
        const channel_id = decodeURIComponent(this.props.match.params.id);
        this.props.load_channel_messages(channel_id);
    }
    
    async handleDownload(event, file) {
        var filenames = file.name.split('/');

        var filename;
        if(0 < filenames.length){
            filename = filenames[filenames.length - 1];
        }
        if(!filename || 0 === filename.length){
            filename = "file";
        }

        const fileStream = createWriteStream(filename, { size: file.size });
        const writer = fileStream.getWriter();

        for (let index = 0; index < file.file_blocks.length; index++) {
            let buffer = await this.props.download(file.file_blocks[index]);
            await writer.write(buffer);
        }

        await writer.close();

/*
        var data = new Blob(['test'], { type: file.mime_type });
        var csvURL = window.URL.createObjectURL(data);
        var tempLink = document.createElement('a');
        tempLink.href = csvURL;
        tempLink.setAttribute('download', filename);
        tempLink.click();
    */}

    render() {
        const { classes } = this.props;
        const files = this.props.vdsApiChannelFiles;
        
        return (
            <Table className={classes.root}>
                <TableHead>
                    <TableRow>
                        <TableCell>File</TableCell>
                        <TableCell align="right">Size</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {Array.from(files, ([key, item]) =>(
                        <TableRow key={key} >
                            <TableCell component="th" scope="row" onClick={(event)=>this.handleDownload(event, item[item.length - 1])}>
                                {key}
                            </TableCell>
                            <TableCell align="right">{item[item.length - 1].size}</TableCell>                                
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        );
    }
}
  
FileStorageFiles.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default connect(
    state => state.vdsApi,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(withStyles(styles, { withTheme: true })(FileStorageFiles));
