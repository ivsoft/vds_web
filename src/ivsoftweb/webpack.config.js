const autoprefixer = require('autoprefixer');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
	'bundle': ['./index.scss', './index.js'],
	'products/vds/get_started/bundle': ['./products/vds/get_started/index.scss', './products/vds/get_started/index.js'],
	'products/vds/how_it_works/bundle': ['./products/vds/how_it_works/index.scss', './products/vds/how_it_works/index.js'],
	'products/vds/bundle': ['./products/vds/index.scss', './products/vds/index.js'],
	'products/development/get_started/bundle': ['./products/development/get_started/index.scss', './products/development/get_started/index.js'],
	'products/development/about_us/bundle': ['./products/development/about_us/index.scss', './products/development/about_us/index.js'],
	'products/development/bundle': ['./products/development/index.scss', './products/development/index.js'],
	'products/bundle': ['./products/index.scss', './products/index.js'],
  },
  devServer: {
    port: 8080,
    historyApiFallback: {
      index: 'index.html'
    }
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].css',
            },
          },
          {loader: 'extract-loader'},
          {loader: 'css-loader'},
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer()]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: ['./node_modules'],
              }
            },
          }
        ],
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['@babel/preset-env'],
        },
      }
    ],
  },
  plugins: [
    new CopyPlugin([
      { from: 'index.html', to: '.' },
      { from: 'assets', to: 'assets' },
		 { from: 'products/vds/get_started/index.html', to: 'products/vds/get_started' },
		 { from: 'products/vds/how_it_works/index.html', to: 'products/vds/how_it_works' },
		 { from: 'products/vds/index.html', to: 'products/vds' },
		 { from: 'products/development/get_started/index.html', to: 'products/development/get_started' },
		 { from: 'products/development/about_us/index.html', to: 'products/development/about_us' },
		 { from: 'products/development/index.html', to: 'products/development' },
		 { from: 'products/index.html', to: 'products' },
    ]),
  ],
};